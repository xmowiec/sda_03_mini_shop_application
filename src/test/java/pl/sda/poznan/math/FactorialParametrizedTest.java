package pl.sda.poznan.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class FactorialParametrizedTest {

    private int n; //number to count factorial of it
    private long fact; //factorial of n

    /**
     * Test constructor
     *
     * @param n    number to count factorial of it
     * @param fact factorial of n
     */
    public FactorialParametrizedTest(int n, long fact) {
        this.n = n;
        this.fact = fact;
    }

    @Parameters
    public static Collection<Object[]> setData() {
        //suffix 'L' to initialize long value
        return Arrays.asList(new Object[][]{
                {1, 1},
                {2, 2},
                {3, 6},
                {4, 24},
                {5, 120},
                {18, 6_402_373_705_728_000L}
        });
    }

    @Test
    public void shouldCountFactorial() {
        // AAA
        // Act
        long result = Factorial.factorial(this.n);
        assertEquals(this.fact, result);

    }
}
