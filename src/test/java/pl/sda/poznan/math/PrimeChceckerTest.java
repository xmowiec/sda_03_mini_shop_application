package pl.sda.poznan.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PrimeChceckerTest {

    private int number;
    private boolean isPrime;

    public PrimeChceckerTest(int number, boolean isPrime) {
        this.number = number;
        this.isPrime = isPrime;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {2, true},
                {3, true},
                {4, false},
                {5, true},
                {6, false},
                {7, true}
        });
    }


    @Test
    public void isPrime() throws Exception {
        assertEquals(PrimeChcecker.isPrime(number), isPrime);
    }

}