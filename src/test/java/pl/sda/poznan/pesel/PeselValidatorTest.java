package pl.sda.poznan.pesel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PeselValidatorTest {


    private String pesel; //PESEL number
    private boolean isValid; //PESEL is valid

    public PeselValidatorTest(String pesel, boolean isValid) {
        this.pesel = pesel;
        this.isValid = isValid;
    }

    @Parameters
    public static Collection<Object[]> setData() {
        return Arrays.asList(new Object[][]{
                {"85050611534", true},
                {"80120611058", true},
                {"80061719503", true},
                {"86101117768", true},
                {"79032011647", false},
                {"52102517323", false},
        });
    }

    @Test
    public void peselValid() {
        // AAA
        //Arrange
        PeselValidator peselValidator = new PeselValidator();
        // Act
        boolean result = PeselValidator.peselValid(this.pesel);
        // Assert
        assertEquals(this.isValid, result);


    }

}