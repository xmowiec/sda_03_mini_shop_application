package pl.sda.poznan.mini_shop.model;

import org.junit.Test;
import pl.sda.poznan.mini_shop.iterator.Iterator;

import static org.junit.Assert.*;

public class ShopCartTest {
    @Test
    public void should_iterate_through_cart() {
        ShopCart cart = new ShopCart();

        cart.add(
                CartItem.builder()
                .name("Telefon")
                .unitPrice(100D)
                .build()
        );

        cart.add(
                CartItem.builder()
                .name("Laptop")
                .unitPrice(1000D)
                .build()
        );

        Iterator<CartItem> iterator = cart.getIterator();

        while (iterator.hasNext()){
            CartItem next = iterator.next();
            System.out.println(next);
        }


    }

}