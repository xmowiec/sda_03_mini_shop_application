package pl.sda.poznan.math;

public class Calculator {
    public static int sum(String str) {
        if (str.isEmpty()) {
            return 0;
        }
        if (str.length() == 1) {
            return Integer.parseInt(str);
        }
        int sum = 0;
        String[] split = str.split(",");

        for (String s : split) {
            sum += Integer.parseInt(s);
        }
        return sum;
    }
}
