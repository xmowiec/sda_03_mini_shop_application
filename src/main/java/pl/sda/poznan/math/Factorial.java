package pl.sda.poznan.math;

public class Factorial {


    /**
     * Best practices
     * Don't initialize object
     */
    private Factorial() {
        throw new IllegalStateException("Utility class");
    }

    public static long factorial(int n) {
//        throw new UnsupportedOperationException();
        if (n == 0) {
            return 1;
        } else {
            return factorial(n-1)*n;
        }
    }
}
