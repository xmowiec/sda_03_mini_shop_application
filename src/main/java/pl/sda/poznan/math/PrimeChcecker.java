package pl.sda.poznan.math;

import java.util.Scanner;

public class PrimeChcecker {

    public static void dividers(int number) {
        int sqrt = (int) Math.sqrt(number);
        System.out.println("Pierwiastek z " + number + " = " + sqrt);
        for (int i = 1; i <= sqrt; i++) {
            if (number % i == 0) {
                System.out.print(String.format("%4d",i));
            }
        }
        System.out.println();
        for (int i = number; i > sqrt; i--) {
            if (number % i == 0) {
                System.out.print(String.format("%4d",i));
            }
        }
    }

    public static boolean isPrime(int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.print("Podaj liczbę: ");
        Scanner scanner = new Scanner(System.in);
        dividers(scanner.nextInt());
    }
}
