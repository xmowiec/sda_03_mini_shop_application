package pl.sda.poznan.pesel;

public class PeselValidator {


    public static boolean peselValid(String pesel) {

        int checksum =
                9 * (digitValue(pesel, 0)) +
                        7 * (digitValue(pesel, 1)) +
                        3 * (digitValue(pesel, 2)) +
                        1 * (digitValue(pesel, 3)) +
                        9 * (digitValue(pesel, 4)) +
                        7 * (digitValue(pesel, 5)) +
                        3 * (digitValue(pesel, 6)) +
                        1 * (digitValue(pesel, 7)) +
                        9 * (digitValue(pesel, 8)) +
                        7 * (digitValue(pesel, 9));

        return digitValue(pesel, 10) == checksum % 10;
    }

    private static int digitValue(String pesel, int index) {
        return pesel.charAt(index) - 48;
    }
}
