package pl.sda.poznan.mini_shop.file.product;

import pl.sda.poznan.mini_shop.model.Product;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class TxtProductWriter implements ProductFileWriter {

    private String path;

    /**
     * Contructor used to set path to txt file
     *
     * @param path
     */
    public TxtProductWriter(String path) {
        this.path = path;
    }

    @Override
    public void saveToFile(List<Product> elements) throws IOException {
        // todo write list of products to txt file
        List<String> collect = elements.stream()
                .map(Product::toString)
                .collect(Collectors.toList());
        Files.write(Paths.get(path), collect);
    }
}
