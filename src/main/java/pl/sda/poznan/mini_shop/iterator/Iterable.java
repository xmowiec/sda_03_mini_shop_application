package pl.sda.poznan.mini_shop.iterator;

public interface Iterable<T> {

    Iterator<T> getIterator();
}
