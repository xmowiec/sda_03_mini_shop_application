package pl.sda.poznan.mini_shop.repository;

import pl.sda.poznan.mini_shop.exceptions.ProductNotFoundException;
import pl.sda.poznan.mini_shop.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductRepository {

    /**
     * Private constructor to deny create more objects
     */
    private ProductRepository() {
    }

    /**
     * Singleton pattern
     */
    private static ProductRepository instance = new ProductRepository();

    /**
     * @return instance of product repository (the only one)
     */
    public static ProductRepository getInstance() {
        return instance;
    }

    // initialize ArrayList of Products
    private List<Product> products = new ArrayList<>();

    private static Long productId = 1L;

    public Product add(Product product) {
        //set next id for product
        product.setId(productId++);
        this.products.add(product);
        return product;
    }

    /**
     * Add all the products from argument list to this product repository
     *
     * @param products
     */
    public void add(List<Product> products) {
/*
        for (Product product : products) {
            this.add(product);
        }
*/
//        products.forEach(product -> this.add(product));
        products.forEach(this::add);
    }

    public List<Product> getAllProducts() {
        return this.products;
    }

    /**
     * Imperative style
     *
     * @param id
     * @return
     */
    public Product getById(Long id) {

        // lambda expression (anonymous function)
        // class Optional, method from class Supplier
        return this.products
                .stream()
                .filter(p -> p.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new ProductNotFoundException("Nie ma produktu o takim identyfikatorze"));
    }

    public Product getByName(String name) {

        return this.products
                .stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new ProductNotFoundException("Nie ma produktu o takiej nazwie"));
    }

    public boolean remove(Long id) {
        return this.products.removeIf(p -> p.getId().equals(id));
    }

    public boolean remove(Product product) {
        return this.products.remove(product);
    }

    public int count() {
        return this.products.size();
    }
}
