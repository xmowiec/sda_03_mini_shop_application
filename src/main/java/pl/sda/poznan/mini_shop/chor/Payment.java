package pl.sda.poznan.mini_shop.chor;

public interface Payment {

    boolean handle(Double amount);
    void setNextHandler (Payment nextHandler);
}
