package pl.sda.poznan.mini_shop.chor;

public abstract class BasePayment implements Payment {

    protected Account account;
    protected Payment nextPayment;

    public BasePayment(Account account) {
        this.account = account;
    }

    @Override
    public boolean handle(Double amount) {
        if (canHandle(amount)) {
            return pay(amount);
        } else if (nextPayment != null) {
            return nextPayment.handle(amount);
        }
        return false;
    }

    @Override
    public void setNextHandler(Payment nextHandler) {
        this.nextPayment = nextHandler;

    }

    protected abstract boolean canHandle(Double amount);

    // todo service this payment
    protected abstract boolean pay(Double amount);
}
