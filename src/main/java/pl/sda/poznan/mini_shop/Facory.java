package pl.sda.poznan.mini_shop;

public interface Facory<K, V> {
    V create(K key);
}
