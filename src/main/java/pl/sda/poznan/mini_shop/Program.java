package pl.sda.poznan.mini_shop;

import pl.sda.poznan.mini_shop.model.CartItem;
import pl.sda.poznan.mini_shop.model.Product;
import pl.sda.poznan.mini_shop.model.ShopCart;
import pl.sda.poznan.mini_shop.repository.ProductRepository;

import java.util.Arrays;
import java.util.Scanner;


public class Program {

    private static ShopCart cart;

    public static void main(String[] args) {
        cart = new ShopCart();
        initializeApp();
        boolean exit = false;
        while (!exit) {
            printMenu();

            Scanner scanner = new Scanner(System.in);
            int userChoice = scanner.nextInt();
            switch (userChoice) {
                case 1:
                    showItems();
                    break;
                case 2:
                    logInUser();
                    break;
                case 3:
                    registerUser();
                    break;
                case 0:
                    exit = exitApp();
                    break;
            }

        }
    }

    private static void printMenu() {
        System.out.println("\r\nMenu:\r\n");
        System.out.println("1. Wyświetl dostępne produkty");
        System.out.println("2. Dodawanie do koszyka");

        System.out.println("9. Logowanie użytkownika");
        System.out.println("10. Rejestracja użytkownika");
        System.out.println("0. Wyjście ze sklepu");
        System.out.println();

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();

        switch (option) {
            case 1: {
                showItems();
                break;
            }
            case 2: {
                addToCart();
                break;
            }
            case 3: {
                logInUser();
                break;
            }
        }

    }

    private static void showItems() {
        System.out.println("== Lista produktów: ==");
        ProductRepository.getInstance()
                .getAllProducts()
//                .forEach(System.out::println);
                .forEach(p -> System.out.println(p.toString()));
        System.out.println("== Koniec listy ==");
    }

    private static void addToCart() {
        System.out.println("== Dodawanie do koszyka ==\r\n");
        System.out.println("Podaj id produktu");
        Scanner scanner = new Scanner(System.in);
        Long result = scanner.nextLong();
        Product product = ProductRepository.getInstance().getById(result);
        System.out.println("Wybrany produkt: " + product.toString());
        System.out.print("Podaj ilość: ");
        int quantity = scanner.nextInt();
        //build new cart item to add to cart
        CartItem build = CartItem.builder()
                .name(product.getName())
                .description(product.getDescription())
                .unitPrice(product.getPrice())
                .quantity(quantity)
                .build();
        cart.add(build);
    }

    public static void logInUser() {
        System.out.println("== Logowanie uzytkownika ==");
        System.out.print("Podaj nazwę użytkownika: ");
        System.out.print("Podaj hasło użytkownika: ");
    }

    public static void registerUser() {
        System.out.println("== Rejestracja użytkownika ==");
        System.out.println("Wypełnij wszystkie pola:");

    }

    public static boolean exitApp() {
        System.out.println("Wyjście.");
        System.out.println("Czy na pewno chcesz wyjść ze sklepu? [T/N]");
        Scanner scanner = new Scanner(System.in);
        String userChoice = scanner.nextLine();
        return userChoice.toUpperCase().equals("T");
    }

    private static void seedProducts() {
        Product smartphone = new Product("Nokia", "Nokia 3310", 150D);
        Product laptop = new Product("Dell", "Ultrabook", 1500D);
        Product concole = new Product("Xbox", "One X", 2000D);

        ProductRepository.getInstance()
                .add(Arrays.asList(concole, laptop, smartphone));

    }

    private static void initializeApp() {
        seedProducts();
    }
}

