package pl.sda.poznan.mini_shop;

import pl.sda.poznan.mini_shop.file.product.JsonProductWriter;
import pl.sda.poznan.mini_shop.file.product.ProductFileWriter;
import pl.sda.poznan.mini_shop.file.product.TxtProductWriter;
import pl.sda.poznan.mini_shop.file.product.XmlProductWriter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

public class ProductFileWriterFactory implements Facory<String, ProductFileWriter> {


    Map<String, ProductFileWriter> writerMap = new HashMap<>();

    Map<Predicate<String>, Function<String, ProductFileWriter>> map = new HashMap<>();


    public ProductFileWriterFactory() {
//        writerMap.put(".txt", new TxtProductWriter("?"));
//        writerMap.put(".xml", new XmlProductWriter("?"));
//        writerMap.put(".json", new JsonProductWriter("?"));

//        map.put(s -> s.endsWith(".txt"),s -> new TxtProductWriter(s));
//        map.put(s -> s.endsWith(".xml"),s -> new XmlProductWriter(s));
//        map.put(s -> s.endsWith(".json"),s -> new JsonProductWriter(s));
//
        map.put(s -> s.endsWith(".txt"), TxtProductWriter::new);
        map.put(s -> s.endsWith(".xml"), XmlProductWriter::new);
        map.put(s -> s.endsWith(".json"), JsonProductWriter::new);

    }

    /**
     * V - value
     *
     * @param path K
     * @return
     */
    @Override
    public ProductFileWriter create(String path) {



        ProductFileWriter productFileWriter = writerMap.get(path);

        return null;
    }
}
